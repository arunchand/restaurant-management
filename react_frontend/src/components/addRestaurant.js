// External Dependencies
import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
// import { Link, useHistory } from "react-router-dom";

// Internal Dependencies
import {
  _restaurantName_validator,
  _managerName_validator,
  _contactNumber_validator,
  _menu_validator,
  _timings_validator,
} from "./inputValidators.js";
import "./App.css";

// Declaring Function AddRestaurant to add New Restaurant Info
function AddRestaurant() {
  // Declaring history object
  // const history = useHistory;
  const navigate = useNavigate();
  // Declaring state
  const [restaurantName, setRestaurantName] = useState("");
  const [managerName, setManagerName] = useState("");
  const [contactNumber, setContactNumber] = useState("");
  const [menu, setMenu] = useState("");
  const [timings, setTimings] = useState("");
  const [headStatus, setHeadStatus] = useState("");
  // Declaring state for error messages
  const [restaurantNameError, setRestaurantNameError] = useState("");
  const [managerNameError, setManagerNameError] = useState("");
  const [contactNumberError, setContactNumberError] = useState("");
  const [menuError, setMenuError] = useState("");
  const [timingsError, setTimingsError] = useState("");

  /**
   * restaurantName
   * onchangeName function takes the event value
   * it will set state value and error value for restaurantName.
   * @param {*} event
   */
  const onChangerestaurantName = (event) => {
    setRestaurantName(event.target.value);
    setRestaurantNameError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validaterestaurantName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurrestaurantName = (event) => {
    validaterestaurantName(event.target.value);
  };
  // Validates the name and set error if any
  const validaterestaurantName = (restaurantName) => {
    setRestaurantNameError(_restaurantName_validator(restaurantName));
  };

  /**
   * managerName
   * onchangeName function takes the event value
   * it will set state value and error value for restaurantName.
   * @param {*} event
   */
  const onChangemanagerName = (event) => {
    setManagerName(event.target.value);
    setManagerNameError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurmanagerName = (event) => {
    validateManagerName(event.target.value);
  };
  // Validates the name and set error if any
  const validateManagerName = (managerName) => {
    setManagerNameError(_managerName_validator(managerName));
  };

  /**
   * ContactNumber
   * onchangeName function takes the event value
   * it will set state value and error value for contactNumber.
   * @param {*} event
   */
  const onChangeContactNumber = (event) => {
    setContactNumber(event.target.value);
    setContactNumberError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurContactNumber = (event) => {
    validateContactNumber(event.target.value);
  };
  // Validates the name and set error if any
  const validateContactNumber = (contactNumber) => {
    setContactNumberError(_contactNumber_validator(contactNumber));
  };

  /**
   * Menu
   * onchangeName function takes the event value
   * it will set state value and error value for Menu.
   * @param {*} event
   */
  const onChangeMenu = (event) => {
    setMenu(event.target.value);
    setMenuError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurMenu = (event) => {
    validateMenu(event.target.value);
  };
  // Validates the name and set error if any
  const validateMenu = (menu) => {
    setMenuError(_menu_validator(menu));
  };

  /**
   * Timings
   * onchangeName function takes the event value
   * it will set state value and error value for Menu.
   * @param {*} event
   */
  const onChangeTimings = (event) => {
    setTimings(event.target.value);
    setTimingsError("");
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurTimings = (event) => {
    validateTimings(event.target.value);
  };
  // Validates the name and set error if any
  const validateTimings = (timings) => {
    setTimingsError(_timings_validator(timings));
  };

  // Decalring Reset Errors
  const resetErrors = () => {
    setRestaurantNameError("");
    setManagerNameError("");
    setContactNumberError("");
    setMenuError("");
    setTimingsError("");
  };
  // Declaring Reset Feilds
  const resetFields = () => {
    setRestaurantName("");
    setManagerName("");
    setContactNumber("");
    setMenu("");
    setTimings("");
  };
  //time out function
  const clearStatus = () => {
    setHeadStatus("");
  };
  //submitting the form
  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validaterestaurantName(restaurantName);
    validateManagerName(managerName);
    validateContactNumber(contactNumber);
    // validateMenu(menu);
    validateTimings(timings);

    // Throws error if any feild is missing
    if (
      _restaurantName_validator(restaurantName).length > 0 ||
      _managerName_validator(managerName).length > 0 ||
      _contactNumber_validator(contactNumber).length > 0 ||
      // _menu_validator(menu).length > 0 ||
      _timings_validator(timings).length > 0
    ) {
      console.log("ERROR");
      return;
    } else {
      const restaurant_Info = {
        restaurantName,
        managerName,
        contactNumber,
        // menu,
        timings,
      };
      const url = "http://localhost:3001/addRestaurant";
      const options = {
        method: "POST",
        body: JSON.stringify(restaurant_Info),
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };

      try {
        const response = await fetch(url, options);
        const info = await response.json();
        console.log(info);
        if (info.message === "Restaurant Info Added") {
          console.log("added");
          setHeadStatus(info.message);
          setTimeout(clearStatus, 4000);
          resetFields();
          navigate("/");
        } else {
          setHeadStatus(info.message);
          setTimeout(clearStatus, 5000);
        }
      } catch (error) {
        // unknown error
        setHeadStatus("Unable to contact server.");
      }
    }
  };
  return (
    <div className="form_addrestaurant">
      <form>
        <h2>Add New Restaurant</h2>
        <div className="inner-div-add">
          <div className="input-label-div">
            <label>Restaurant Name : </label>
            <input
              type="text"
              id="restaurantName"
              name="restaurantName"
              value={restaurantName}
              onChange={(event) => onChangerestaurantName(event)}
              onBlur={(event) => onBlurrestaurantName(event)}
              required
              placeholder="Restaurant Name"
            />
          </div>
          <div className="error-msg">{restaurantNameError}</div>
          <div className="input-label-div">
            <label>Manager Name : </label>
            <input
              type="text"
              id="managerName"
              name="managerName"
              value={managerName}
              onChange={(event) => onChangemanagerName(event)}
              onBlur={(event) => onBlurmanagerName(event)}
              required
              placeholder="Manager Name"
            />
          </div>
          <div className="error-msg">{managerNameError}</div>
          <div className="input-label-div">
            <label>Contact Number : </label>
            <input
              type="number"
              id="contactNumber"
              name="Contact Number"
              value={contactNumber}
              onChange={(event) => onChangeContactNumber(event)}
              onBlur={(event) => onBlurContactNumber(event)}
              required
              placeholder="Contact number"
            />
          </div>
          <div className="error-msg">{contactNumberError}</div>
          <div className="input-label-div">
            <label>Menu : </label>
            <input
              type="text"
              id="menu"
              name="menu"
              value={menu}
              onChange={(event) => onChangeMenu(event)}
              onBlur={(event) => onBlurMenu(event)}
              required
              placeholder="Menu"
            />
          </div>
          <div className="error-msg">{menuError}</div>
          <div className="input-label-div">
            <label>Timings : </label>
            <input
              type="text"
              id="timings"
              name="timings"
              value={timings}
              onChange={(event) => onChangeTimings(event)}
              onBlur={(event) => onBlurTimings(event)}
              required
              placeholder="Timings"
            />
          </div>
          <div className="error-msg">{timingsError}</div>
        </div>
        <input
          type="submit"
          id="add-btn"
          value="ADD"
          onClick={submitForm}
        />
        <h2 className="headstatus">{headStatus}</h2>
      </form>
    </div>
  );
}

export default AddRestaurant;
