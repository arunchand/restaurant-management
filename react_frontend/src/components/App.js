import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Header from "./header.js";
import AddRestaurant from "./addRestaurant";
import RestaurantList from "./home";
import UpdateRestaurant from "./updateRestaurant";
import MenuItem from "./menu";


function App() {
  return (
    <div>
      <Router>
        <Header />
        <Routes>
          <Route path="/" element={<RestaurantList />} />
          <Route path="/menu" element={<MenuItem/>} />
          <Route path="/addRestaurant" element={<AddRestaurant />} />
          <Route path="/updateRestaurant" element={<UpdateRestaurant/>} />
        </Routes>
      </Router>
    </div>
  );
}

export default App;
