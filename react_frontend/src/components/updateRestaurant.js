// External Dependencies
import React, { useState } from "react";
import { useNavigate, useLocation } from "react-router-dom";

// Internal Dependencies
import "./App.css";
import {
  _restaurantName_validator,
  _managerName_validator,
  _contactNumber_validator,
  _timings_validator,
} from "./inputValidators.js";
function UpdateRestaurant() {
  // Declaring history object
  // const history = useHistory;
  const navigate = useNavigate();
  const location = useLocation();
  const restaurant = location.state.restaurant;
  // Declaring state
  const [restaurantName, setRestaurantName] = useState(
    restaurant.restaurantName
  );
  const [managerName, setManagerName] = useState(restaurant.managerName);
  const [contactNumber, setContactNumber] = useState(restaurant.contactNumber);
  const [menu, setMenu] = useState(restaurant.menu);
  const [timings, setTimings] = useState(restaurant.timings);
  const [headStatus, setHeadStatus] = useState("");

  // Declaring state for error messages
  const [restaurantNameError, setRestaurantNameError] = useState("");
  const [managerNameError, setManagerNameError] = useState("");
  const [contactNumberError, setContactNumberError] = useState("");
  const [timingsError, setTimingsError] = useState("");

  /**
   * restaurantName
   * onchangeName function takes the event value
   * it will set state value and error value for restaurantName.
   * @param {*} event
   */
  const onChangerestaurantName = (event) => {
    setRestaurantName(event.target.value);
    setRestaurantNameError("");
  };

  /**
   * OnBlur function takes the event value as a param
   * calls the validaterestaurantName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurrestaurantName = (event) => {
    validaterestaurantName(event.target.value);
  };
  // Validates the name and set error if any
  const validaterestaurantName = (restaurantName) => {
    setRestaurantNameError(_restaurantName_validator(restaurantName));
  };
  /**
   * managerName
   * onchangeName function takes the event value
   * it will set state value and error value for restaurantName.
   * @param {*} event
   */
  const onChangemanagerName = (event) => {
    setManagerName(event.target.value);
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurmanagerName = (event) => {
    validateManagerName(event.target.value);
  };
  // Validates the name and set error if any
  const validateManagerName = (managerName) => {
    setManagerNameError(_managerName_validator(managerName));
  };

  /**
   * ContactNumber
   * onchangeName function takes the event value
   * it will set state value and error value for contactNumber.
   * @param {*} event
   */
  const onChangeContactNumber = (event) => {
    setContactNumber(event.target.value);
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurContactNumber = (event) => {
    validateContactNumber(event.target.value);
  };
  // Validates the name and set error if any
  const validateContactNumber = (contactNumber) => {
    setContactNumberError(_contactNumber_validator(contactNumber));
  };

  /**
   * Menu
   * onchangeName function takes the event value
   * it will set state value and error value for Menu.
   * @param {*} event
   */
  const onChangeMenu = (event) => {
    setMenu(event.target.value);
  };

  /**
   * Timings
   * onchangeName function takes the event value
   * it will set state value and error value for Menu.
   * @param {*} event
   */
  const onChangeTimings = (event) => {
    setTimings(event.target.value);
  };
  /**
   * OnBlur function takes the event value as a param
   * calls the validatemanagerName function to validate the param
   * throws error if any
   * @param {*} event
   */
  const onBlurTimings = (event) => {
    validateTimings(event.target.value);
  };
  // Validates the name and set error if any
  const validateTimings = (timings) => {
    setTimingsError(_timings_validator(timings));
  };

  // Decalring Reset Errors
  const resetErrors = () => {
    setRestaurantNameError("");
    setManagerNameError("");
    setContactNumberError("");
    setTimingsError("");
  };

  //time out function
  const clearStatus = () => {
    setHeadStatus("");
  };

  const submitForm = async (event) => {
    event.preventDefault();
    resetErrors();
    validaterestaurantName(restaurantName);
    validateManagerName(managerName);
    validateContactNumber(contactNumber);
    validateTimings(timings);

    // Throws error if any feild is missing
    if (
      _restaurantName_validator(restaurantName).length > 0 ||
      _managerName_validator(managerName).length > 0 ||
      _contactNumber_validator(contactNumber).length > 0 ||
      _timings_validator(timings).length > 0
    ) {
      console.log("ERROR");
      return;
    }
    const restaurant_Info = {
      restaurantName,
      managerName,
      contactNumber,
      menu,
      timings,
    };
    const url = "http://localhost:3001/updateInfo";
    const options = {
      method: "PUT",
      body: JSON.stringify(restaurant_Info),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };

    try {
      const response = await fetch(url, options);
      const info = await response.json();
      console.log(info);
      if (info.message === "Restaurant Info updated successfully") {
        console.log("added");
        setHeadStatus(info.message);
        setTimeout(clearStatus, 2000);
        // resetFields();
        navigate("/");
      } else {
        setHeadStatus(info.message);
        setTimeout(clearStatus, 5000);
      }
    } catch (error) {
      // unknown error
      setHeadStatus("Unable to contact server.");
    }
  };
  return (
    <div>
      <div>
        <form className="form_addrestaurant">
          <h2 className="heading">Update Restaurant Info</h2>
          <div className="inner-div-add">
            <div className="input-label-div">
              <label>Restaurant Name : </label>
              <input
                type="text"
                id="restaurantName"
                name="restaurantName"
                value={restaurantName}
                onChange={(event) => onChangerestaurantName(event)}
                onBlur={(event) => onBlurrestaurantName(event)}
                required
                placeholder="Restaurant Name"
              />
            </div>
            <div>{restaurantNameError}</div>
            <div className="input-label-div">
              <label>Manager Name : </label>
              <input
                type="text"
                id="managerName"
                name="managerName"
                value={managerName}
                onChange={(event) => onChangemanagerName(event)}
                onBlur={(event) => onBlurmanagerName(event)}
                placeholder="Manager Name"
              />
            </div>
            <div>{managerNameError}</div>
            <div className="input-label-div">
              <label>Contact Number : </label>
              <input
                type="number"
                id="contactNumber"
                name="Contact Number"
                value={contactNumber}
                onChange={(event) => onChangeContactNumber(event)}
                onBlur={(event) => onBlurContactNumber(event)}
                placeholder="contact number"
              />
            </div>
            <div>{contactNumberError}</div>
            <div className="input-label-div">
              <label>Menu : </label>
              <input
                type="text"
                id="menu"
                name="menu"
                value={menu}
                onChange={(event) => onChangeMenu(event)}
                placeholder="menu"
              />
            </div>
            <div className="input-label-div">
              <label>Timings : </label>
              <input
                type="text"
                id="timings"
                name="timings"
                value={timings}
                onChange={(event) => onChangeTimings(event)}
                onBlur={(event) => onBlurTimings(event)}
                placeholder="Timings"
              />
            </div>
            <div>{timingsError}</div>
            <input
              type="submit"
              id="save-btn"
              value="Save"
              className="add_info_button"
              onClick={submitForm}
            />
          </div>
          <h2 className="headstatus">{headStatus}</h2>
        </form>
      </div>
    </div>
  );
}

export default UpdateRestaurant;
