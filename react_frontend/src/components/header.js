import React from "react";

const Header = () => {
  return (
    <div className="header">
      <div>
        <h1>Restaurant Management</h1>
      </div>
    </div>
  );
};

export default Header;
