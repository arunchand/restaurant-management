// import React from "react";
import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

const RestaurantList = () => {
  const [info, setInfo] = useState([]);
  const [searchdata, setSearchdata] = useState([]);
  const [searchName, setSearchName] = useState("");
  const [responseError, setResponseError] = useState("");

  const searchChangeHandler = (e) => {
    setSearchName(e.target.value);
    setSearchdata("");
    setResponseError("");
  };

  const SearchHandler = async (e) => {
    if (searchName === "") return;
    const url = "http://localhost:3001/getRestaurant";
    const options = {
      method: "POST",
      body: JSON.stringify({ restaurantName: searchName }),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    try {
      const response = await fetch(url, options);
      const data = await response.json();
      if (data.status === 404) {
        console.log("error123",responseError)
        return setResponseError(data.message);
      }
      setSearchdata(data);
      console.log("SearchData", searchdata);
    } catch {
      console.log("error");
    }
  };

  const DeleteHandler = async (e) => {
    const name = e.target.value;
    const url = "http://localhost:3001/deleteRestaurant";
    const options = {
      method: "DELETE",
      body: JSON.stringify({ restaurantName: name }),
      headers: {
        "Content-Type": "application/json",
        Accept: "application/json",
      },
    };
    try {
      const response = await fetch(url, options);
      const data = await response.json();
    } catch {
      setResponseError("info not found");
    }
  };

  useEffect(() => {
    async function fetch_data() {
      const url = "http://localhost:3001/getRestaurants";
      const options = {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
        },
      };
      try {
        const response = await fetch(url, options);
        const data = await response.json();
        setInfo(data);
      } catch {
        console.log("error in finding ");
      }
    }
    fetch_data();
  }, [DeleteHandler]);
  return (
    <div>

      {/* Add restaurant button and Search button */}
      <div className="add-rest-btn-div">
        <Link to="/addRestaurant">
          <button className="add-rest-button">Add Restaurant</button>
        </Link>
        <div className="search-div">
          <input
            type="text"
            id="search"
            name="searchRestaurant"
            placeholder="Search Restaurant"
            onChange={(e) => searchChangeHandler(e)}
          />
          <button className="search-btn" onClick={(e) => SearchHandler(e)}>
            Search
          </button>
        </div>
      </div>
      {/* Error message if restaurant not found */}
      <h3 className="responseError">{responseError}</h3>

      {/* Searched Items List  */}
      <div className="searched-items-list">
        {searchdata.length > 0
          ? searchdata.map((item) => {
              return (
                <div className="searched-List-card">
                  <h4> Restaurant Name : {item.restaurantName}</h4>
                  <p>
                    manager Name : <strong>{item.managerName}</strong>
                  </p>
                  Contact Number : <strong>{item.contactNumber}</strong>
                  <p className="menu-show-more">
                    Menu : <strong>{item.menu}</strong>
                  </p>
                  <p>
                    Timings : <strong>{item.timings}</strong>
                  </p>
                  <div className="buttons">
                    <Link
                      to="/updateRestaurant"
                      state={{ restaurant: searchdata }}
                    >
                      <button className="edit-btn">Edit</button>
                    </Link>
                    <button
                      className="delete-btn"
                      value={searchdata.restaurantName}
                      onClick={(e) => DeleteHandler(e)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              );
            })
          : ""}
      </div>
      {searchdata.length > 0 ? (
        ""
      ) : (

        // -----Restaurant List------------
        <div className="list-container">
          {info.map((item) => {
            return (
              <div>
                <div className="restaurantList-card">
                  <h4>Restaurant Name : {item.restaurantName}</h4>
                  <p>
                    Manager Name :<strong>{item.managerName}</strong>
                  </p>
                  Contact Number : <strong>{item.contactNumber}</strong>
                  {/* <p className="menu-show-more">
                    Menu :{" "}
                    <strong>{item.menu.map((item) => item + ",")}</strong>
                  </p> */}
                  <Link to="/menu" state={{restaurantName: item.restaurantName}}>
                    <button>menu</button>
                  </Link>
                  <p>
                    Timings : <strong>{item.timings}</strong>
                  </p>
                  <div className="buttons">
                    <Link to="/updateRestaurant" state={{ restaurant: item }}>
                      <button className="edit-btn">Edit</button>
                    </Link>
                    <button
                      className="delete-btn"
                      value={item.restaurantName}
                      onClick={(e) => DeleteHandler(e)}
                    >
                      Delete
                    </button>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default RestaurantList;
