// Internal Dependencies

/**
 * validator function to validate Restaurant Name.
 * checks if restaurantName is string, else returns "restaurantName should be of string type"
 * checks if restaurantName is too short, else returns "restaurantName should be at least 2 characters minimum and max 15"
 * @param {} restaurantName
 * @returns
 */
const _restaurantName_validator = (restaurantName) => {
   
  if (typeof restaurantName == "undefined") {
    return;
  }
  if (typeof restaurantName != "string") {
    return "restaurantName should be of string type";
  }
  restaurantName = restaurantName.toString();
  if (restaurantName.length < 2) {
    return "restaurantName length should be at least 2 characters long";
  }

  if (restaurantName.length > 20) {
    return "restaurantName length should not be greater than 20 characters long";
  }

  return "";
};

/**
 * validator function to validate Restaurant Name.
 * checks if managerName is string, else returns "managerName should be of string type"
 * checks if managerName is too short, else returns "managerName should be at least 2 characters minimum and max 15"
 * @param {} managerName
 * @returns
 */
const _managerName_validator = (managerName) => {
  if (typeof managerName == "undefined") {
    return;
  }
  if (typeof managerName !== "string") {
    return "managerName should be of string type";
  }
  managerName = managerName.toString();
  if (managerName.length < 2) {
    return "managerName length should be at least 2 characters long";
  }

  if (managerName.length > 15) {
    return "managerName length should not be greater than 15 characters long";
  }

  return "";
};

/**
 * validator function to validate contact number
 * checks if contact Number is number, else returns "contact number should be of number type"
 * checks if contact Number is too short, else returns "contact number should be at least 1 characters long and max of 10"
 * checks if contact Number is of the correct format, else returns "contact number should be of the correct format" 406
 * @param {} contactNumber
 * @returns
 */
const _contactNumber_validator = (contactNumber) => {
  contactNumber = contactNumber.toString();

  // if (typeof contactNumber != "number") {
  //   return "contact number should be of number type";
  // }

  if (contactNumber.length !== 10) {
    return "contact number length should be 10 digits long";
  }
  return "";
};

/**
 * validator function to validate menu
 * checks if menu is string, else returns "menu should be of number type"
 * checks if menu is too short, else returns "menu should be at least 3 characters long and max of 20"
 * @param {} menu
 * @returns
 */
const _menu_validator = (menu) => {

  if (typeof menu != "string") {
    return "menu should be of string type";
  }
  menu = menu.toString();
  if (menu.length < 3) {
    return "menu length should be at least 3 characters long";
  }
  if (menu.length > 200) {
    return "menu length should not be greater than 200 characters long";
  }
  return "";
};

/**
 * validator function to validate timings.
 * checks if timings is string, else returns "timings should be of string type"
 * checks if timings is too short, else returns "timings should be at least 4 characters minimum and max 15"
 * @param {} timings
 * @returns
 */
const _timings_validator = (timings) => {
  if (typeof timings == "undefined") {
    return;
  }
  if (typeof timings != "string") {
    return "timings should be of string type";
  }
  timings = timings.toString();
  if (timings.length < 4) {
    return "timings length should be at least 4 characters long";
  }
  if (timings.length > 15) {
    return "timings length should not be greater than 15 characters long";
  }

  return "";
};

// Exporting validator functions
export {
  _restaurantName_validator,
  _managerName_validator,
  _contactNumber_validator,
  _menu_validator,
  _timings_validator,
};
