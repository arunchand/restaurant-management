// External Dependencies
import React, { useState, useEffect } from "react";
import { useNavigate, useLocation } from "react-router-dom";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions } from "@mui/material";


function MenuItem(){
    const [menuInfo, setMenuInfo] = useState([]);
    const location = useLocation();
    const {restaurantName} = location.state
    console.log(restaurantName)
      useEffect(() => {
        async function fetch_data() {
          const url = "http://localhost:3001/get_menu";
          const options = {
            method: "POST",
            body: JSON.stringify({ restaurantName }),
            headers: {
              "Content-Type": "application/json",
              Accept: "application/json",
            },
          };
          try {
            const response = await fetch(url, options);
            const data = await response.json();
            setMenuInfo(data.data);
            console.log("menudata",data.data)
            console.log("menuinfo",menuInfo)
          } catch {
            console.log("error in finding ");
          }
        }
        fetch_data();
      }, []);

    return (
        menuInfo.length>0?menuInfo.map((item)=>{
            <Card sx={{ maxWidth: 200 }}>
              <CardActionArea>
                <CardMedia
                  component="img"
                  height="200"
                  image="/images/menu-icon.png"
                  alt="green iguana"
                />
                <CardContent>
                  <Typography gutterBottom variant="h5" component="div">
                    {item.itemName}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    Price : {item.itemPrice}
                  </Typography>
                </CardContent>
              </CardActionArea>
              <CardActions>
                <Button size="small" color="primary">
                  Add to Cart
                </Button>
              </CardActions>
            </Card>;
        }):""
    );

}

export default MenuItem;