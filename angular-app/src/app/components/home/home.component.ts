import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RestaurantsService } from 'src/app/service/restaurants.service';
import { MatIconModule, MatIconRegistry } from '@angular/material/icon';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { PopupComponent } from '../popup/popup.component';
import { Restaurant } from '@material-ui/icons';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  restaurants: any;
  // restaurantName:string;
  constructor(
    private dialog: MatDialog,
    private service: RestaurantsService,
    public dialogRef: MatDialogRef<PopupComponent>
  ) {}
  ngOnInit(): void {
    this.getRestaurantData()
  }

  getRestaurantData() {
    this.service.GetRestData().subscribe((result) => {
      this.restaurants = result;
      console.log('All restaurants data', this.restaurants);
    });
  }

  DeleteRestaurantInfo(restaurantName: any) {
    this.service
      .DeleteRestaurant(restaurantName)
      .subscribe((data) => {
        this.ngOnInit()
      });
     
  }
  // @Output() restaurantName = new EventEmitter<string>()

  openDialog(restaurantName: any): void {
    const dialogRef = this.dialog.open(PopupComponent, {
      width: '250px',
      data: { restaurantName },
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      result = console.log('The dialog was closed');
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}