import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantsService } from 'src/app/service/restaurants.service';

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.css'],
})
export class UpdateRestaurantComponent implements OnInit {
  UpdateDataInfo: any;

  constructor(private service: RestaurantsService, private router: Router) {
    this.UpdateDataInfo = this.router.getCurrentNavigation()?.extras.state;
  }
  ngOnInit(): void {}

  getRestaurantName(event: any) {
    this.UpdateDataInfo.restaurant.restaurantName = event.target.value;
  }
  getManagerName(event: any) {
    this.UpdateDataInfo.restaurant.managerName = event.target.value;
  }
  getContactNumber(event: any) {
    this.UpdateDataInfo.restaurant.contactNumber = event.target.value;
  }
  // getMenu(event: any) {
  //   this.UpdateDataInfo.restaurant.menu = event.target.value;
  // }
  getTimings(event: any) {
    this.UpdateDataInfo.restaurant.timings = event.target.value;
  }

  UpdateRestaurantData() {
    // console.log(event,"Event")
    const name = this.UpdateDataInfo.restaurant.restaurantName
    this.service
      .UpdateInfo(this.UpdateDataInfo.restaurant)
      .subscribe((data) => console.log('subscribe data', data));
  }
}
