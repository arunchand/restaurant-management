import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Message } from '@material-ui/icons';
import { RestaurantsService } from 'src/app/service/restaurants.service';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.css'],
})
export class AddRestaurantComponent implements OnInit {
  restaurantInfo = {
    restaurantName: '',
    managerName: '',
    contactNumber: '',
    timings: '',
  };
  errorMessage: any;
  msg: any;
  constructor(private service: RestaurantsService, private router: Router) {}

  ngOnInit(): void {}

  getRestaurantName(event: any) {
    this.restaurantInfo.restaurantName = event.target.value;
  }
  getManagerName(event: any) {
    this.restaurantInfo.managerName = event.target.value;
  }
  getContactNumber(event: any) {
    this.restaurantInfo.contactNumber = event.target.value;
  }
  // getMenu(event: any) {
  //   this.restaurantInfo.menu = event.target.value;
  // }
  getTimings(event: any) {
    this.restaurantInfo.timings = event.target.value;
  }

  addRestaurantInfo() {
    this.service.NewRestaurant(this.restaurantInfo).subscribe((data) => {
      console.log(data);
      this.errorMessage = data;
      this.msg = this.errorMessage.message;
      this.router.navigate(['']);
    });
  }
}
