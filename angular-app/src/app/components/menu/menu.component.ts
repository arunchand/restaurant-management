import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';
import { RestaurantsService } from 'src/app/service/restaurants.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css'],
})
export class MenuComponent implements OnInit {
  menu:any;
  restaurantName:any;
  constructor(private service: RestaurantsService, private router:Router) {
    this.restaurantName = this.router.getCurrentNavigation().extras.state['restaurantName']
    
    this.service
      .GetMenuData({restaurantName:this.restaurantName})
      .subscribe((data) =>
        console.log(
          'data',
          (this.menu = data)
        )
      );
  }

  ngOnInit(): void {}
}
