import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestaurantsService } from 'src/app/service/restaurants.service';

@Component({
  selector: 'app-add-menu',
  templateUrl: './add-menu.component.html',
  styleUrls: ['./add-menu.component.css'],
})
export class AddMenuComponent implements OnInit {
  resName : any
  menuData = {
    restaurantName: "",
    itemName: '',
    itemPrice: '',
  };
  errorMessage: any;
  msg:any;
  constructor(private service: RestaurantsService,private router:Router) {
      this.resName= this.router.getCurrentNavigation()?.extras.state
      this.menuData.restaurantName = this.resName.restaurantName
  }

  ngOnInit(): void {}

  getRestaurantName(event: any) {
    this.menuData.restaurantName = event.target.value;
  }
  getItemName(event: any) {
    this.menuData.itemName = event.target.value;
  }
  getItemPrice(event: any) {
    this.menuData.itemPrice = event.target.value;
  }
  addMenuInfo() { 
    this.service
      .AddMenu(this.menuData)
      .subscribe((data) =>{ 
       this.errorMessage = data;
       this.msg = this.errorMessage.message;
       this.router.navigate(['']);
      // if (this.msg === 'menu added successfully') {
        //  setTimeout(()=> this.router.navigateByUrl(''),3000);
      //  }
    });

  }
}
