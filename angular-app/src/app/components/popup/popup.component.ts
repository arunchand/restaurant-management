import { Component, OnInit, Input, Inject } from '@angular/core';
import {
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { Restaurant } from '@material-ui/icons';
import { RestaurantsService } from 'src/app/service/restaurants.service';
@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.css'],
})
export class PopupComponent implements OnInit {
  menu : any
  menuInfo = {
    restaurantName: this.data.restaurantName,
  };
  // constructor() { }
  // @Input restaurantName
  ngOnInit(): void {}

  constructor(
    public dialogRef: MatDialogRef<PopupComponent>,
    private service: RestaurantsService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.service
      .GetMenuData(this.data)
      .subscribe((data) =>
        console.log(
          'data',
          (this.menu = data)
        )
      );
      
  }
  

  onNoClick(): void {
    this.dialogRef.close();
  }

  // getMenuInfo() {
  //   console.log("restaurantName",this.data)
    
  // }
}
