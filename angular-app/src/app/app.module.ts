// External Modules
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {
  MatDialogModule,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { AddRestaurantComponent } from './components/add-restaurant/add-restaurant.component';
import { UpdateRestaurantComponent } from './components/update-restaurant/update-restaurant.component';
import { PopupComponent } from './components/popup/popup.component';
import { AddMenuComponent } from './components/add-menu/add-menu.component';
import { MenuComponent } from './components/menu/menu.component';



const appRoute: Routes = [
  { path: '', component: HomeComponent },
  { path: 'addRestaurant', component: AddRestaurantComponent },
  { path: 'update', component: UpdateRestaurantComponent },
  { path: 'menu', component: MenuComponent },
  { path: 'menu/addmenu', component: AddMenuComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    AddRestaurantComponent,
    UpdateRestaurantComponent,
    PopupComponent,
    AddMenuComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    MatDialogModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MatTooltipModule,
    RouterModule.forRoot(appRoute)
  ],
  
  providers: [MatDialogModule, { provide: MAT_DIALOG_DATA, useValue: {} }, { provide: MatDialogRef, useValue: {} }],
  bootstrap: [AppComponent]
})
export class AppModule { }
