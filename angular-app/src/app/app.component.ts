import { Component } from '@angular/core';
// import { MatDialog } from '@angular/material/dialog';
import { PopupComponent } from './components/popup/popup.component';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'angular-app';

  // restaurants: any;
  // constructor(private service: RestaurantsService){
  //   this.service.GetRestData().subscribe(result =>{
  //     this.restaurants = result;
  //     console.log(this.restaurants);
  //   });

  constructor(private dialog: MatDialog) {}
  // openDialog(){
  // this.dialogRef.open(PopupComponent);
  //   // }

  // }

  // openDialog(): void {
  //   const dialogRef = this.dialog.open(PopupComponent, {
  //     width: '250px'
  //   });

  //   dialogRef.afterClosed().subscribe((result) => {
  //     console.log('The dialog was closed');
  //   });
  // }

}
