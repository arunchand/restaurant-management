import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class RestaurantsService {
  restaurants: any;
  baseUrl: string = `http://localhost:3001`;
  constructor(private http: HttpClient) {}

  GetRestData() {
    return this.http.get(`${this.baseUrl}/getRestaurants`);
  }
  GetMenuData(menuInfo: any) {
    return this.http.post(`${this.baseUrl}/get_menu`, menuInfo);
  }
  NewRestaurant(restaurantInfo: any) {
    return this.http.post(`${this.baseUrl}/addRestaurant`, restaurantInfo);
  }
  AddMenu(menuData: any) {
    return this.http.post(`${this.baseUrl}/add_menu`, menuData);
  }
  UpdateInfo(updateData: any){
    return this.http.put(`${this.baseUrl}/updateInfo`,updateData);
  }
  DeleteRestaurant(restaurantName:any){
    
     const options = {
       headers: new HttpHeaders({
         'Content-Type': 'application/json',
       }),
       body: {
         restaurantName: restaurantName,
       },
     };
    
    return this.http.delete<any>(`${this.baseUrl}/deleteRestaurant`, options).pipe(res=>{
      return res
    });
  }
}
