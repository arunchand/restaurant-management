// External Dependencies
import mongoose from "mongoose";
// Internal Dependencies
import { RestaurantInfoSchema } from "./schema.js";

// Connecting to the MongoDB database
mongoose.connect(process.env.MONGODB_LINK);

// Creating and exporting Model for Restaurant Information

export const RestaurantInfoModel =
  mongoose.models.RestaurantInfoModel ||
  mongoose.model("RestaurantInfoModel", RestaurantInfoSchema);
