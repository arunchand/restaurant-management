//External Dependencies
import mongoose from "mongoose";

// Creating Schema for Restaurant Information
const RestaurantInfoSchema = new mongoose.Schema({
  restaurantName : { type: String, required: true },
  managerName    : { type: String, required: true },
  contactNumber  : { type: Number, required: true },
  menu           : [{itemName:String, itemPrice:Number}],
  timings        : { type: String, required: true },
});

// Exporting RestaurantInfoSchema
export { RestaurantInfoSchema };
