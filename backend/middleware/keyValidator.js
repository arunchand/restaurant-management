function _keys_validator(request, response, next) {
  const keys = request.body;
  const keylist = [];
  for (const [key, value] of Object.entries(keys)) {
    if (
      !(
        key == "restaurantName" ||
        key == "managerName" ||
        key == "contactNumber" ||
        key == "timings"
      )
    ) {
      return response
        .status(406)
        .send(`Please provide the exact key. The key:${key} is incorrect`);
    }
    if (keylist.includes(key.toLowerCase())) {
      return res.status(406).send(`${key} key is repeated`);
    }
    keylist.push(key.toLowerCase());
  }
  if (keylist.length < 4) {
    return response.status(406).send(`${4 - keylist.length} key(s) missing`);
  }
  if (keylist.length > 4) {
    return res.status(406).send(`${keylist.length - 4} keys extra`);
  }
  next();
}

export { _keys_validator };
