// Internal Dependencies

/**
 * Helper function to validate Restaurant Name.
 * checks if restaurantName is string, else returns "restaurantName should be of string type"
 * checks if restaurantName is too short, else returns "restaurantName should be at least 2 characters minimum and max 15"
 * @param {} restaurantName
 * @returns
 */
const _restaurantName_helper = (restaurantName) => {
  
  const returnObject = {};
  if (typeof restaurantName == "undefined") {
    return;
  }
  if (typeof restaurantName != "string") {
    returnObject.restaurantName = "restaurantName should be of string type";
    return returnObject;
  }
  restaurantName = restaurantName.toString();
  if (restaurantName.length < 2) {
    returnObject.restaurantName =
      "restaurantName length should be at least 2 characters long";
    return returnObject;
  }

  if (restaurantName.length > 20) {
    returnObject.restaurantName =
      "restaurantName length should not be greater than 20 characters long";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate Restaurant Name.
 * checks if managerName is string, else returns "managerName should be of string type"
 * checks if managerName is too short, else returns "managerName should be at least 2 characters minimum and max 15"
 * @param {} managerName
 * @returns
 */
const _managerName_helper = (managerName) => {
  const returnObject = {};
  if (typeof managerName == "undefined") {
    return;
  }
  if (typeof managerName !== "string") {
    returnObject.managerName = "managerName should be of string type";
    return returnObject;
  }
  managerName = managerName.toString();
  if (managerName.length < 2) {
    returnObject.managerName =
      "managerName length should be at least 2 characters long";
    return returnObject;
  }

  if (managerName.length > 15) {
    returnObject.managerName =
      "managerName length should not be greater than 15 characters long";
    return returnObject;
  }

  return returnObject;
};

/**
 * Helper function to validate contact number
 * checks if contact Number is number, else returns "contact number should be of number type"
 * checks if contact Number is too short, else returns "contact number should be at least 1 characters long and max of 10"
 * checks if contact Number is of the correct format, else returns "contact number should be of the correct format" 406
 * @param {} contactNumber
 * @returns
 */
const _contactNumber_helper = (contactNumber) => {
  const returnObject = {};

  contactNumber = contactNumber.toString();
  if (typeof contactNumber != "string") {
    returnObject.contactNumber = "contact number should be of number type";
    return returnObject;
  }

  if (contactNumber.length !== 10) {
    returnObject.contactNumber =
      "contact number length should be 10 digits long";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate menu
 * checks if menu is string, else returns "menu should be of number type"
 * checks if menu is too short, else returns "menu should be at least 3 characters long and max of 20"
 * @param {} menu
 * @returns
 */
const _menu_helper = (menu) => {
  const returnObject = {};

  if (typeof menu != "string") {
    returnObject.menu = "menu should be of string type";
    return returnObject;
  }
  menu = menu.toString();
  if (menu.length < 3) {
    returnObject.menu = "menu length should be at least 3 characters long";
    return returnObject;
  }
  if (menu.length > 200) {
    returnObject.menu =
      "menu length should not be greater than 200 characters long";
    return returnObject;
  }
  return returnObject;
};

/**
 * Helper function to validate timings.
 * checks if timings is string, else returns "timings should be of string type"
 * checks if timings is too short, else returns "timings should be at least 4 characters minimum and max 15"
 * @param {} timings
 * @returns
 */
const _timings_helper = (timings) => {
  const returnObject = {};
  if (typeof timings == "undefined") {
    return;
  }
  if (typeof timings != "string") {
    returnObject.timings = "timings should be of string type";
    return returnObject;
  }
  timings = timings.toString();
  if (timings.length < 4) {
    returnObject.timings =
      "timings length should be at least 4 characters long";
    return returnObject;
  }

  if (timings.length > 15) {
    returnObject.timings =
      "timings length should not be greater than 15 characters long";
    return returnObject;
  }

  return returnObject;
};

// Exporting helper functions
export {
  _restaurantName_helper,
  _managerName_helper,
  _contactNumber_helper,
  _menu_helper,
  _timings_helper,
};
