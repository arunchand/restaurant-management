import {
  _restaurantName_helper,
  _managerName_helper,
  _contactNumber_helper,
  _timings_helper,
} from "../middleware/helpers.js";

/**
 * This is a middleware function to help with validation of Info.
 * parameters.
 * @param {}  request   HTTP req object as recieved by Express backend
 * @param {*} response   HTTP response object to be populated by Express backend
 * @param {*} next  next() is a function to be called if this middlware validation is success
 * @returns
 */
const _info_validator = (request, response, next) => {
  const { restaurantName, managerName, contactNumber, timings } =
    request.body;
  const error_object = {};

  error_object.restaurantName = _restaurantName_helper(restaurantName).restaurantName;
  error_object.managerName    = _managerName_helper(managerName).managerName;
  error_object.contactNumber  = _contactNumber_helper(contactNumber).contactNumber;
  error_object.timings        = _timings_helper(timings).timings;

  for (const [key, value] of Object.entries(error_object)) {
    if (value) {
      return response.status(200).send(error_object);
    }
  }
  next();
};

export { _info_validator };
