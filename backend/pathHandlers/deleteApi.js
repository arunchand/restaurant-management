// Internal Dependencies
import { response } from "express";
import { RestaurantInfoModel } from "../utils/model.js";

/**
 * @param delete_restaurantInfo deletes the details of the products
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

const delete_restaurantInfo = (request, response) => {
  const { restaurantName } = request.body;
  RestaurantInfoModel.findOneAndRemove(
    { restaurantName: restaurantName },
    (err, data) => {
      if (!restaurantName) {
        return response.status(400).send("name not found");
      }
      if (err) {
        return response.status(401).send({ message: "Error in deleting" });
      }
      if (!err) {
        response.send({ message: "Restaurant Info deleted successfully" });
      }
    }
  );
};

//delete menu handler
const delete_menu_handler = (request, response) => {
  const { restaurantName, itemName } = request.body;
  console.log(restaurantName, itemName);
  RestaurantInfoModel.find({ restaurantName: restaurantName }, (err, data) => {
    if (err) {
      return response.status(401).send({ message: "Error in deleting" });
    }
    if (!err) {
      RestaurantInfoModel.findOneAndDelete(
        { menu:menu },
        (err, data) => {
          if (err) {
            
            return response.status(404).send({ message: "item not found" });
          }
          if (!err) {
            console.log(restaurantName);
            return response.send({ message: "item deleted successfully" });
          }
        }
      );
    }
  });
};

//exporting delete_restaurantInfo module

export { delete_restaurantInfo, delete_menu_handler };
