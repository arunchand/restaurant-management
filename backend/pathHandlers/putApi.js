// Internal Dependencies
import { RestaurantInfoModel } from "../utils/model.js";

/**
 * @param update_restaurantInfo_handler deletes the details of the products
 * @param {*} request HTTP req object as recieved by Express backend
 * @param {*} response HTTP response object to be populated by Express backend
 */

function update_restaurantInfo_handler(request, response) {
  const { restaurantName, managerName, contactNumber, timings } =
    request.body;
  RestaurantInfoModel.findOne({ restaurantName: restaurantName }, (err, data) => {
    if (err) {
      response.status(404).send({ message: "Cannot find "+restaurantName+" in database" });
    } else {
      if (data) {
        RestaurantInfoModel.updateOne(
          { restaurantName: restaurantName },
          {
            restaurantName: restaurantName,
            managerName: managerName,
            contactNumber: contactNumber,
            timings: timings,
          }
        )
          .then(() => {
            response
              .status(200)
              .send({ message: "Restaurant Info updated successfully" });
          })
          .catch(() => {
            response
              .status(304)
              .send({ message: "Failed to update in the database" });
          });
      } else {
        response.status(401).send({ message: "Resturant not found" });
      }
    }
  });
}

export default update_restaurantInfo_handler;