import { RestaurantInfoModel } from "../utils/model.js";

const get_restaurantInfo_handler = (request, response) => {
  // const jwt_token = request.headers.authorization.split(" ")[1];

  RestaurantInfoModel.find({}, (err, data) => {
    if (err) {
      response
        .status(404)
        .send({
          error: "Unable to fetch Restaurants Info in database",
        });
    } else {
      response.send(data);
    }
  });
};


const get_specific_restaurantInfo_handler = (request, response) => {
  const name = request.body.restaurantName;

  RestaurantInfoModel.find(
    { restaurantName: { $regex: `${name}` }},
    (err, data) => {
      if (err) {
        return response
          .status(404)
          .send({ error: "error connecting to database" });
      }
      if (!data[0]) {
        return response.status(404).send({
          status: 404,
          message: "Restaurant is not there in database",
        });
      } else if (data) {
        response.status(200).send(data);
      }
    }
  );
};

const get_menu_handler = (request, response) => {
  const name = request.body.restaurantName;
  RestaurantInfoModel.find({ restaurantName: name }, (err, data) => {
    if (err) {
      return response.send({ status: 404, error: "menu not found" });
    }
    if(!data[0]){
      return response.send({ status: 404, error: "restaurant not found" });
    }
    else if ((data[0].menu.length==0)) {
      return response.send({
        status: 404,
        error: "menu not found for this restaurant",
      });
    } else if (data[0].menu.length > 0) {
      return response.send({ status: 200, data: data[0].menu });
      
    } 
    else {
      return response.status(401).send({
        status: 401,
        message: "No menu found for this restaurant",
      });
    }
  });
}

export {get_restaurantInfo_handler,get_specific_restaurantInfo_handler, get_menu_handler } ; 