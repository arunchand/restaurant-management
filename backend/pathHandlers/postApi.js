// External Dependencies
import { request, response } from "express";
import mongoose from "mongoose";
// Internal Dependencies
import { RestaurantInfoModel } from "../utils/model.js";

/**
 * This function is used to check whether the Restaurant Info is posted or not.
 * If the Restaurant Info registered successfully then it will send the status_message
 * @param {*} request body params from body
 * @param {*} response sends statuscode and status_message as response for successfull registeration '201'
 * @param {*} next it is an express middleware
 */
function RestaurantInfo_handler(request, response) {
  const { restaurantName, managerName, contactNumber, timings } =
    request.body;
  const restaurantInfo = {
    restaurantName,
    managerName,
    contactNumber,
    timings
  };

  RestaurantInfoModel.find(
    {
      restaurantName: { $regex: restaurantName, $options: "i" },
    },
    (err, data) => {
      if (err) {
        return response.status(406);
      }
      if (data.length === 0) {
        const restaurantdata = new RestaurantInfoModel(restaurantInfo);
        restaurantdata.save().then(() => {
          return response
            .status(201)
            .send({ message: "Restaurant Info Added" });
        });
      } else {
        return response.status(409).send({
          message: "Restaurant name already exists, Try another one!",
        });
      }
    }
  );
}

function Add_menu_handler(request, response) {
  const {restaurantName} = request.body;
  const { itemName, itemPrice } = request.body;
  // console.log(menu)
  RestaurantInfoModel.findOneAndUpdate({restaurantName: restaurantName},{$push:{menu:{itemName,itemPrice}}},(err,data)=>{
    if(err){
       return response.status(404).send({message: "error in adding menu.",err})
    }
    else{ return response.send({status:200,message: "menu added successfully"})}
  })
}


export { RestaurantInfo_handler, Add_menu_handler };
