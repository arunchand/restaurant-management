// External Dependencies
import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import "dotenv/config";

// Internal Dependencies
import { _keys_validator } from "./middleware/keyValidator.js";
import { _info_validator } from "./middleware/infoValidator.js";
import { Add_menu_handler, RestaurantInfo_handler } from "./pathHandlers/postApi.js";
import {
  get_restaurantInfo_handler,
  get_menu_handler,
  get_specific_restaurantInfo_handler,
} from "./pathHandlers/getApi.js";
import {
  delete_restaurantInfo,
  // delete_menu_handler,
} from "./pathHandlers/deleteApi.js";
import update_restaurantInfo_handler from "./pathHandlers/putApi.js";


//Creating an instance for express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
//enable the express server to respond to preflight requests
app.use(cors({ origin: "*" }));


/*--------------------POST CALLS--------------------------- */

// Post call for adding Restaurant.
app.post(
  "/addRestaurant",
  _keys_validator,
  _info_validator,
  RestaurantInfo_handler
);
// Post call for getting SPECIFIC Restaurant Info.
app.post("/getRestaurant", get_specific_restaurantInfo_handler);
// Post call for adding MENU
app.post("/add_menu", Add_menu_handler)

/*---------------------GET CALLS--------------------------- */

// Get call for getting Restaurant Info List.
app.get("/getRestaurants", get_restaurantInfo_handler);
// Get call for getting MENU from specific Restaurant Info.
app.post("/get_menu", get_menu_handler);

/*---------------------UPDATE CALLS------------------------ */

// Update call for updating Restaurant Info
app.put("/updateInfo", update_restaurantInfo_handler);

/*---------------------DELETE CALLS------------------------ */

// Delete call for Deleting particular Restaurant Info.
app.delete("/deleteRestaurant", delete_restaurantInfo);
// Delete menu
// app.delete("/delete_menu", delete_menu_handler)

app.listen(process.env.BACKEND_PORT, () => {
  console.log("http://localhost:3001");
});
